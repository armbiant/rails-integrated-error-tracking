Rails.application.routes.draw do
  resources :posts do
    collection do
      get :error
      get :error2
      get :error3
      get :random_error
    end
  end

  get 'welcome/index'
  root 'posts#random_error'
end
